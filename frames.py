#!/usr/bin/python3

import pyatspi
import Xlib
import Xlib.display
import PIL.Image
import sys

if len(sys.argv) <= 1:
    print("Usage: frames.py output.png")
    sys.exit(1)

disp = Xlib.display.Display()
screen = disp.screen()

screen_width = screen.width_in_pixels
screen_height = screen.height_in_pixels

output = PIL.Image.new("RGB", (screen_width, screen_height), (255, 255, 255))

def fix(a, limit):
    if a < 0:
        return 0
    if a >= limit:
        return limit-1
    return a

def fixX(x):
    return fix(x, screen_width)

def fixY(y):
    return fix(y, screen_height)


def rectangle(i, x1, x2, y1, y2):
    x1 = fixX(x1)
    x2 = fixX(x2)
    y1 = fixY(y1)
    y2 = fixY(y2)
    for x in range(x1, x2+1):
        i.putpixel((x, y1), (0, 0, 0))
        i.putpixel((x, y2), (0, 0, 0))

    for y in range(y1+1, y2):
        i.putpixel((x1, y), (0, 0, 0))
        i.putpixel((x2, y), (0, 0, 0))

def draw_access(access):
    component = access.queryComponent()
    (x, y, width, height) = component.getExtents(0)
    print("draw %dx%d+%d+%d\n" % (width, height, x, y))
    rectangle(output, x, x+width-1, y, y+height-1)

def draw_elements(access):
    if not access.getState().contains(pyatspi.STATE_SHOWING):
        return
    print("found element", access.getRole())
    if access.getRole() in \
            [ pyatspi.ROLE_CHECK_BOX, 
              pyatspi.ROLE_PUSH_BUTTON, 
              pyatspi.ROLE_TOGGLE_BUTTON,

              pyatspi.ROLE_TREE_TABLE,
              pyatspi.ROLE_TABLE,

              pyatspi.ROLE_LABEL, 
              pyatspi.ROLE_ENTRY,
              pyatspi.ROLE_TEXT,

              pyatspi.ROLE_FRAME ]:
        print("draw it")
        draw_access(access)
    for child in access:
        draw_elements(child)

desktop = pyatspi.Registry.getDesktop(0)
for app in desktop:
    print(app.name)
    for frame in app:
        print(frame)
        draw_elements(frame)

output.save(sys.argv[1])
